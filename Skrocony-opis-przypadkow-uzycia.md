Opis skrócony przypadków użycia
===============
- Sprawdzenie stanu rachunku
- Drukowanie potwierdzeń
- Wypłacanie gotówki z rachunku
- Wpłacenie gotówki na rachunek
- Dokonanie jednej z pow. operacji bez odebrania karty z bankomatu

Wymagania techniczne
---------------
- Bankomat posiada wyświetlacz dotykowy
- Bankomat posiada klawiaturę fizyczną


Odbiorcy systemu
---------------

#### Odbiorca Cel 

- Bank:          Sprawdzenie stanu rachunku
- Admin.:      Wypłacenie gotówki z rachunku
- Serwis.:      Wpłacenie gotówki na rachunek


Aktorzy procesu i ich cele
---------------

#### Aktor       Cel 

- Aktor 1:      Sprawdzenie stanu rachunku
- Aktor 2:      Wypłacenie gotówki z rachunku
- Aktor 3:      Wpłacenie gotówki na rachunek


Słownik
---------------

#### Hasło       Opis

- Kod PIN - Ciąg 4 cyfr wybranych przez użytkownika w momencie tworzenia rachunku

- Banknot - Pieniądz papierowy emitowany przez dowolny bank, poświadczający posiadanie przez okaziciela pewnej wartości

- Slot karty - Miejsce do wkładania karty

- Slot gotówkowy - Miejsce do wkładania/odbierania banknotów

- System rozliczeniowy - System informatyczny przechowujący informację dotyczące stanu oraz parametrów (właściciela, kart) każdego rachunku. System rozliczeniowy dokonuje uznań i obciążeń rachunku dzięki mechanizmowi transakcyjnemu

- Stan rachunku - Ilość posiadanych pieniędzy na rachunku

- Klawiatura fizyczna - Klawiatura numeryczna wbudowana w bankomat

- Okno - Przycisk na dotykowym ekranie bankomatu

- Transakcja - operacja pieniężna (uznanie lub obciążenie rachunku) rejestrowana w systemie rozliczeniowym

- Karta płatnicza - karta uprawniająca do wypłaty gotówki lub umożliwiająca złożenie zlecenia płatniczego za pośrednictwem akceptanta lub agenta rozliczeniowego, akceptowana przez akceptanta w celu otrzymania przez niego należnych mu środków


Przypadki użycia
---------------

#### Use case 1: Sprawdzanie stanu rachunku
Użytkownik wkłada kartę do bankomatu, wprowadza PIN, wybiera okno Sprawdzenie stanu rachunku. Po wyborze jednej z opcji za pośrednictwem ekranu dotykowego, stan rachunku zostaje wyświetlony na ekranie lub widnieje na wydrukowanym wyciągu z konta. Po zakończeniu użytkownik odbiera swoją kartę ze slotu karty.

#### Use case 2: Wypłacanie gotówki z rachunku
Użytkownik wkłada kartę do bankomatu, wprowadza PIN, wybiera okno Wypłacanie gotówki z rachunku. Użytkownik wpisuje kwotę za pośrednictwem klawiatury fizycznej i potwierdza. Operacja zostaje potwierdzona przez system rozliczeniowy. Na ekranie zostaje wyświetlony stan rachunku po odliczeniu wybranej kwoty. Gotówka zostaje wysunięta ze slotu gotówkowego. Po odbiorze gotówki użytkownik odbiera swoją kartę.

#### Use case 3: Wpłacenie gotówki na rachunek
Użytkownik wakłada kartę do bankomatu, wprowadza PIN, wybiera okno Wypłata gotówki z rachunku. Użytkownik wpisuje kwotę za pośrednictwem klawiatury fizycznej i potwierdza. Operacja zostaje potwierdzona przez system rozliczeniowy. Na ekranie zostaje wyświetlony stan rachunku po doliczeniu wybranej kwoty. Bankomat otwiera slot gotówkowy i użytkownik wkłada banknoty. Po przeliczeniu gotówki przez bankomat - urządzenie z powodzeniem wydaje kartę za pośrednictwem slotu karty oraz wyświetla o pomyślnym przebiegu transakcji na ekranie.


Do pełnego opisu: Opcje Alternatywne
---------------

- #### Use case x: Karta jest uszkodzona

- #### Use case x: Dokonanie jednej z pow. operacji bez odebrania karty z bankomatu

- #### Use case x: Dokonanie wypłaty gotówki bez odebrania gotówki (oraz karty z bankomatu)

- #### Use case x: Dokonanie wypłaty gotówki bez odebrania gotówki oraz karty z bankomatu

