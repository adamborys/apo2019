Use Case 2: Wypłacanie gotówki z rachunku
=====================

**Aktor podstawowy:** 
- klient chcący wypłacić gotówke z bankomatu


Główni odbiorcy i oczekiwania względem systemu:
-----------------------------------------------

- klient banku: Oczekuje szybkiej wypłaty gotówki

- Właściciel bankomatu: Działanie bezawaryjne bakomatu

- Bank: chce żeby klient był zadowolony, a transakcje odbywały się bez problemowo 

Warunki wstępne:
----------------

- klient banku ma aktywne konto

- klient banku ma gotówkę na banku

Warunki końcowe:
----------------

- wypłacono gotówkę oraz oddano kartę

Scenariusz główny (ścieżka podstawowa):
---------------------------------------

  1. klient wkłada kartę do bankomatu
  2. klient wpisuje kod PIN
  3. klient wybiera z panelu opcje wypłaty pieniędzy
  4. klient wybiera sumę jaką chce wypłacić
  5. klient odbiera kartę
  6. klient odbiera pieniądze
  7. użytklientkownik kończy sesje

Rozszerzenia (ścieżki alternatywne):
------------------------------------

 1a. karta jest nieaktywna lub nierozpoznana

 1b. klient jest inforowany o niepoprawności karty
 
 2a. klient wpisuje zły PIN mniej niż 3 razy

 2b. klient proszony jest o podanie poprawnego PINu

 2a1. klient wpisuje zły PIN więcej niż 3 razy

 2b1. karta jest blokowana, a na ekranie wyświetla się powiadominie o zatrzymaniu karty 

 4a. klient nie ma wystarczających środków na koncie

 4b. klient informowany jest o zbyt małej liczbie środków

 4b. użytklientkownik proszony jest o podanie mniejszej kwoty

Wymagania specjalne:
--------------------

  - klient posiada gotówkę na koncie w pln

Wymagania technologiczne oraz ograniczenia na wprowadzane dane:
---------------------------------------------------------------

 1a. klient potrzebuje dostępu do bankomatu

 2a. klient potrzebuje karty bankomatowej

 3a. Panel pozwala wybrać odpowienią operację

Kwestie otwarte:
----------------

  - rezygnacja klienta w trakcie przeprowadzania operacji

  - błędy po stronie banku np. nie właściwa ilość pieniędzy w stosunku do posiadanych przez klienta
 
  - błędy w działaniu bankomatu jako maszyny np. spięcie w obwodach albo nagły brak zasilania
