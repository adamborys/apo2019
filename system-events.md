# Zdarzenia systemowe

## Use case 1: Sprawdzanie stanu rachunku
  - Włożenie karty bakomatowej
  - Wyjęcie karty bakomatowej
  - Wprowadzenie kodu PIN
  - Wybór opcji sprawdz stan rachunku na ekranie dotykowym
## Use case 2: Wypłacanie gotówki z rachunku
  - Włożenie karty bakomatowej
  - Wyjęcie karty bakomatowej
  - Wprowadzenie kodu PIN
  - Wybór opcji wypłać gotówkę na ekranie dotykowym
  - Wprowadzenie kwoty
  - Odbiór gotówki
## Use case 3: Wpłacanie gotówki na rachunek
  - Włożenie karty bakomatowej
  - Wyjęcie karty bakomatowej
  - Wprowadzenie kodu PIN
  - Wybór opcji wpłać gotówkę na ekranie dotykowym
  - Wprowadzenie kwoty
  - Włożenie gotówki
## Use case 4: Drukowanie potwierdzenia
  - Włożenie karty bakomatowej
  - Wyjęcie karty bakomatowej
  - Wprowadzenie kodu PIN
  - Wybór opcji wydrukuj potwierdzenie na ekranie dotykowym
