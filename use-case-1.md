# Usecase 1: Sprawdzenie stanu rachunku

## Aktor podstawowy:
Klient posiadający rachunek bankowy i korzystający z usług finansowych za pośrednictwem bankomatu

## Główni odbiorca i ich oczekiwania względem systemu
**Klient**: Oczekuje szybkiego sprawdzenia stanu rachunku bankowego

**Operator bankomatu**: Oczekuje stabilnej i bezawaryjnej pracy systemu bez opóźnień

**Bank**: Oczekuje zadowolenia klienta oraz bezpiecznej realizacji każdej transakcji

## Warunki wstępne
* Klient posiadający kartę chce skorzystać z bankomatu
* Fizyczny bankomat funkcjonuje i wszystkie jego podzespoły są sprawne
* Bankomat jest w stanie przyjąć kartę, odczytać z niej dane oraz zwrócić ją klientowi
* Bankomat jest w stanie oczekiwania na wprowadzenie karty bankomatowej

## Warunki końcowe
* Klient jest poinformowany w kwestii stanu rachunku a bankomat oddał kartę bakomatową
* Bankomat powraca do stanu oczekiwania na wprowadzenie karty bankomatowej

## Scenariusz główny
1. Użytkownik podchodzi i wprowadza kartę bankomatową do slotu
2. Użytkownik jest proszony o PIN za pośrednictwem wyświetlacza
3. Użytkownik wprowadza poprawny PIN zasłaniając ręką klawiaturę fizyczną
4. Użytkownik potwierdza i jest informowany za pośrednictwem wyświetlacza o dostępnych usługach
5. Użytkownik wybiera "Sprawdzanie stanu konta"
6. Użytkownik wybiera jedną spośród dwóch opcji
    * Wyciąg papierowy
    * Wyświetlenie stanu na ekranie
7. Zależnie od wybranej usługi
    * Bankomat drukuje wyciąg, a użytkownik go odbiera ze slotu drukarki
    * Klient dostrzega, rozumie komunikat dotyczący stanu konta i naciska przycisk odpowiadający czynności "Zakończ"
8. Po wykonaniu operacji bankomat ponownie informuje o dostępnych usługach
9. Użytkownik naciska przycisk odpowiadający czynności "Zakończ"
10. Użytkownik odbiera kartę
11. Bankomat wyświetla komunikat zawierający podziękowania za skorzystanie z usług
12. Użytkownik odchodzi od bankomatu

## Rozszerzenia
* Użytkownik wprowadza zły PIN

Bankomat informuje o źle wprowadzonym kodzie PIN i pozostającej jednej szansie podania prawidłowego kodu.
* Użytkownik ponownie wprowadza zły PIN

Bankomat informuje o czasowej blokadzie korzystania z usług bankomatowych i wydaje kartę.
* Bankomat zaprzestaje poprawnie funkcjonować w trakcie korzystania z usług

Użytkownik dzwoni na numer podany na tabliczce/naklejce informującej o możliwościach kontaktu z operatorem oraz usługodawcą.
* Bankomat nie może się połączyć z systemem transakcyjnym

Użytkownik dzwoni na numer podany na tabliczce/naklejce informującej o możliwościach kontaktu z operatorem oraz usługodawcą.
* Przerwa w dostawie prądu

Bankomat jest wyłączony - nic nie wyświetla i uniemożliwia wprowadzanie przedmiotów do slotów.
* Użytkownik nie odbiera karty bankomatowej do 30 sekund po zakończeniu korzystania z usług

Po wspomnianym czasie bankomat zatrzymuje kartę bankomatową i wprowadza ją do depozytu. Użytkownik odbiera kartę od usługodawcy w najbliższym punkcie obsługi klienta.
* Użytkownik nie podejmuje kolejnej czynności do 30 sekund po ostatniej czynności wykonanej podczas korzystania z usług

Po wspomnianym czasie bankomat informuje o zakończeniu usług dla tego wprowadzenia karty oraz o odliczaniu kolejnych 30 sekund do zatrzymania karty bankomatowej. Możliwe są dwa zakończenia tej ścieżki:
1. Użytkownik odbiera kartę do 30 sekund po wyświetleniu komunikatu
2. Karta jest wprowadzana do depozytu. Użytkownik odbiera kartę od usługodawcy w najbliższym punkcie obsługi klienta.

## Wymagania technologiczne oraz ograniczenia na wprowadzane dane

* Działający slot karty bankomatowej
* Działająca drukarka
* Działający ekran i przyciski
* Maksymalnie jedna pomyłka przy wprowadzaniu PIN

## Kwestie otwarte

* Błędy w implementacjach funkcjonalności
* Usterki serwisu transakcyjnego
* Fizyczne uszkodzenie wybranych podzespołów w trakcie korzystania z bankomatu