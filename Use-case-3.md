Use case 3: Wpłacenie gotówki na rachunek
=====================

**Aktor podstawowy:** Klient


Główni odbiorcy i oczekiwania względem systemu:
-----------------------------------------------

- Klient: chęć szybkiej i bezproblemowej wpłaty oraz otrzymania potwierdzenia transakcji.

- Serwisant: chęć szybkiej analizy/diagnozy. Chęć usunięcia usterki w szybki i łatwy sposób. 

- System bankowy: chęć poprawnego obsłużenia transakcji wpłaty przez bankomat. Chęć otrzymania zapytania o potwierdzenie wpłaty.


Warunki wstępne:
----------------

- Bankomat istnieje. Jest sprawny, czyli wszystkie warunki techniczne są spełnione. Działa on poprawnie (czytnik karty, ekran dotykowy). Klient posiada gotówkę do wpłaty oraz kartę płatniczą.

Warunki końcowe:
----------------

- Klient wpłacił odliczoną przez siebie kwotę na rachunek. Wyciągnął kartę płatnicza po wykonanej operacji. Bankomat wydał potwierdzenie wpłaty w postaci fizycznego wydruku.

Scenariusz główny (ścieżka podstawowa):
---------------------------------------

  1. Klient wkłada kartę płatniczą do bankomatu.

  2. Klient podaje PIN do konta.

  3. Bankomat weryfikuje pozytywnie PIN.

  4. Klient wybiera opcję wpłaty gotówki.

  5. Klient umieszcza odliczoną ilość gotówki w otworze bankomatowym.

  6. Klient potwierdza wartość wpłaconej kwoty na ekranie.

  7. Klient odbiera potwierdzenie transakcji.
  

Rozszerzenia (ścieżki alternatywne):
------------------------------------

1. W dowolnym momencie gdy system się zawierza

2a. System dokonuje resetu oraz zwraca wpłacone pieniądze oraz kartę.

2b. System rozpoczyna pracę od nowa.

2c. System wzywa serwisanta.

2d. Serwisant dokonuje wymaganej naprawy.

2e. Serwisant uruchamia system na nowo.

2f. System uruchamia się na nowo.



3. Gotówka jest w złym stanie technicznym.

3a. System informuje o nieodpowiednim stanie gotówki.

3a.1.1 Klient wybiera opcję ponownej wpłaty gotówki.

3a.1.2 Klient wpłaca ponownie gotówkę.

3a.1.3 System ponownie informuje o złym stanie gotówki.

3a.1.4 Klient wraca do punktu 3a.1.2

3a.2 Klient odbiera gotówkę i transakcja jest zakończona.


4.a Klient postanawia wpłacić dodatkową kwotę.

4.a.1 Klient wybiera opcje "dodaj", zamiast zaakceptować widniejącą na ekarnie kwotę.

4.a.2 Klient ponownie wkłada gotówkę do bankomatu.

4.a.3 System ponownie analizuje podaną kwotę.

4.a.3.1 Gdy gotówka jest w złym stanie, system przenosi Klienta do kroku 3.

4.a.3.2 Gdy gotówka jest w dobrym stanie, proces przebiega bez zakłóceń.

4.b Klient rezygnuje z operacji.

4.b.1 Klient rezygnuje z zakupu i wybiera opcję "anuluj". Automat zwraca podaną kwotę i powraca do stanu początkowego.


Wymagania specjalne:
--------------------

- System oferuje dostęp do wielu języków.

- Bankomat oferuje możliwość wpłaty gotówki w różnych walutach.


Wymagania technologiczne oraz ograniczenia na wprowadzane dane:
---------------------------------------------------------------

 1a. Klient musi posiadać kartę płatniczą.

 2a. Czynik kart płatniczyć musi działać.

 3a. Klient musi mieć dostępu do bankomatu.

 4a. Ekran dotykowy musi działać prawidłowo.

 5a. Klient może pomylić się tylko dwa razy przy wpisywaniu kodu PIN.

Kwestie otwarte:
----------------

- Jak system powinien się zachować przy włożeniu podrabianego banknotu?

- Jak bankomat się powinien zachować przy nagłym utracie prądu w trakcie dokonywania transakcji przez klienta?


